# -*- coding: utf-8 -*-

import flask, flask.views
from flask import request
import  predictionio
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
app = flask.Flask(__name__)
@app.route('/index.html',  methods=['GET'])
def index():
    return flask.render_template('index.html', sentence="",
                                 sentiment="")
    
@app.route('/index.html', methods=['POST'])
def sentiment():
    engine_client = predictionio.EngineClient(url="http://104.155.19.140:8000")
        
    text = unicode(str(request.form['query']),encoding="utf8")  
    son = engine_client.send_query({"s" : str(text)})
    sentiment=u""
    if 0<=son['sentiment'] and son['sentiment']<1:
        sentiment=u"Çok Kötü"
    elif 1<=son['sentiment'] and son['sentiment']<2:
        sentiment=u"Kötü"
    elif son['sentiment']==2:
        sentiment=u"Nötür"
    elif 2<son['sentiment'] and son['sentiment']<=3:
        sentiment=u"İyi"
    elif 3<son['sentiment'] and son['sentiment']<=4:
        sentiment=u"Çok İyi"
        
    return flask.render_template('index.html',sentence=text,
                                 sentiment=sentiment)

   
def okan():
     return flask.render_template('Contacts.html')   
if __name__ == "__main__":
    app.debug = True
    app.run()
